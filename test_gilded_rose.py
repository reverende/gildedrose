# -*- coding: utf-8 -*-
import unittest

from gilded_rose import Item, GildedRose


class GildedRoseTest(unittest.TestCase):
    def test_vest_basic(self):
        item = Item(name="+5 Dexterity Vest", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=6, sell_in=4, quality=14)

    def test_vest_negative_sell_in(self):
        item = Item(name="+5 Dexterity Vest", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=12, sell_in=-2, quality=6)

    def test_vest_non_zero_quality(self):
        item = Item(name="+5 Dexterity Vest", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=40, sell_in=-30, quality=0)

    def test_elixir_basic(self):
        item = Item(name="Elixir of the Mongoose", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=6, sell_in=4, quality=14)

    def test_elixir_negative_sell_in(self):
        item = Item(name="Elixir of the Mongoose", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=12, sell_in=-2, quality=6)

    def test_elixir_non_zero_quality(self):
        item = Item(name="Elixir of the Mongoose", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=40, sell_in=-30, quality=0)

    def test_brie_basic(self):
        item = Item(name="Aged Brie", sell_in=10, quality=20)
        self.assert_item_stats_equals(item, days_passed=6, sell_in=4, quality=26)

    def test_brie_negative_sell_in(self):
        item = Item(name="Aged Brie", sell_in=2, quality=20)
        self.assert_item_stats_equals(item, days_passed=4, sell_in=-2, quality=26)

    def test_brie_quality_roof_at_50(self):
        item = Item(name="Aged Brie", sell_in=2, quality=20)
        self.assert_item_stats_equals(item, days_passed=40, sell_in=-38, quality=50)

    def test_sulfuras_negative_sell_in(self):
        item = Item(name="Sulfuras, Hand of Ragnaros", sell_in=10, quality=80)
        self.assert_item_stats_equals(item, days_passed=16, sell_in=10, quality=80)

    def test_passes_basic(self):
        item = Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=14, quality=20)
        self.assert_item_stats_equals(item, days_passed=4, sell_in=10, quality=24)

    def test_passes_lower_sell_in(self):
        item = Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=12, quality=20)
        self.assert_item_stats_equals(item, days_passed=4, sell_in=8, quality=26)

    def test_passes_even_lower_sell_in(self):
        item = Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=7, quality=20)
        self.assert_item_stats_equals(item, days_passed=4, sell_in=3, quality=30)

    def test_passes_missed_concert(self):
        item = Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=20)
        self.assert_item_stats_equals(item, days_passed=6, sell_in=-1, quality=0)

    def test_conjured_basic(self):
        item = Item(name="Conjured Mana Cake", sell_in=5, quality=20)
        self.assert_item_stats_equals(item, days_passed=5, sell_in=0, quality=10)

    def assert_item_stats_equals(self, item, days_passed, sell_in, quality):
        gilded_rose = GildedRose([item])
        for _ in range(days_passed):
            gilded_rose.update_quality()
        self.assertEquals(sell_in, item.sell_in)
        self.assertEquals(quality, item.quality)


if __name__ == '__main__':
    unittest.main()
